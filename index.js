var express = require("express");
var bodyParser = require("body-parser");
var path = require("path");

var app = express();

// var logger = function(req, res, next) {
//   console.log("logging");
//   next();
// };

// app.use(logger);

app.set("view engine", "ejs");

app.set("views", path.join(__dirname, "views"));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, "public")));

var person = [
  { id: 1, name: "Ace", age: 20 },
  { id: 2, name: "jana", age: 5 },
  { id: 3, name: "jade", age: 3 }
];

app.get("/", function(req, res) {
  res.render("index", {
    title: "Customer",
    user: person
  });
});

port = process.env.PORT || 80;
app.listen(3000, function() {
  console.log("server started on port 3000...");
});
